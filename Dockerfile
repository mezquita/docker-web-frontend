# Copyright 2016 The Kubernetes Authors All rights reserved.
# 
# docker run -it -p 80:80 nginx:alpine /bin/bash

FROM nginx:alpine

COPY nginx.conf /etc/nginx/nginx.conf
VOLUME /var/socks/
EXPOSE 80 443

CMD ["/usr/sbin/nginx", "-g", "daemon off;"]
